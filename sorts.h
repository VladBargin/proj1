#ifndef SORTS_H_
#define SORTS_H_
#include <math.h>

void dummy(int A[]);
void merge_sort(int A[], int B[], int n);
void bottom_up_merge(int A[], int iLeft, int iRight, int iEnd, int B[]);
void bubble_sort(int A[], int size);
void shell_sort1(int A[], int B[], int size);
void shell_sort2(int A[], int B[], int size);
void insertion_sort(int A[], int size);
void counting_sort(int A[], int B[], int size);
void quicksort(int A[], int size);
void dual_pivot_quicksort(int a[], int left,int right, int divid);
void heapsort(int A[], int size);
void heapify(int A[], int size);
void sift_down(int A[], int i, int end);
int leaf_search(int A[], int i, int end);
void tim_sort(int A[], int B[], int n);
void ins_sort(int A[], int left, int right);
//void t_s(int A[], int B[], int l, int r);

#endif