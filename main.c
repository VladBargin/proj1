#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "sorts.h"
#include "getMem.h"
#include "utils.h"

#define ACC 1000000
#define TC 100

void warmup(int n);

double callTest(int size, int sortAlg, int testIndex, double args[], int smallN);

double testRandom(int size, int sortAlg, int smallN);
double testSorted(int size, int sortAlg, int smallN);
double testPartiallySorted(int size, int sortAlg, double sortedPercentage, int smallN);
double testSwaps(int size, int sortAlg, double numberOfSwaps, int smallN);
double testPartiallySorted2(int size, int sortAlg, double stepSize, int smallN);
double testSortedBackwards(int size, int sortAlg, int smallN);
double testEqual(int size, int sortAlg, int smallN);
double testPartiallySorted3(int size, int sortAlg, double numOfSteps, int smallN);

double sortPrecall(int A[], int size, int sortAlg, int smallN);

int main(){
    char str[100];
    int n = 1000;
    int y;
    int i = 0;
    int ti;
    int r = 100;
    int st;
    int mult;
    double params[2];
    //params[0] = 0.99;
    params[0] = 0.5;
    printf( "Enter array size: ");
    fflush(stdout);
    scanf("%i", &y);
    fflush(stdin);
    printf( "Enter sortAlg index: ");
    fflush(stdout);
    scanf("%i", &i);
    fflush(stdin);
/*     printf( "Enter test index: ");
    fflush(stdout);
    scanf("%i", &ti);
    fflush(stdin); */
/*     printf( "Enter parram: ");
    fflush(stdout);
    scanf("%lf", &params[0]);
    fflush(stdin); */
    printf( "Enter reps: ");
    fflush(stdout);
    scanf("%i", &r);
    fflush(stdin);    
    printf( "Enter step: ");
    fflush(stdout);
    scanf("%i", &st);
    fflush(stdin);
    printf( "Enter mult: ");
    fflush(stdout);
    scanf("%i", &mult);
    fflush(stdin);
    int c = (y/st) + 1;
    int nList[2000];
    for (int z = 0; z < c; z++) {
        nList[z] = z*st;
    }
    int stL = 0;
    int inCr = n;
    int ind = 0;
    int sn = 2;
    ti = 0;
    while (ind < c) {
        srand(time(NULL)); 
        //warmup(n);`

        int Res[r];
        //params[0] = n / 100;
        for (int j = 0; j < r; j += 1){
            Res[j] = callTest(nList[ind], i, ti, params, sn);
        }
        double s = 0;
        for (int k = 0; k < r; k += 1){
            s += Res[k];
        }
        
        printf("%lf\n", (s*mult) / (r * ACC));
        fflush(stdout);
        ind += 1;
    }
    //printf("\nSort took: %f seconds", callTest(50, 5, 0, params) / ACC);
}

void warmup(int n){
    for (int i = 0; i < n; i++){
        int j = n;
        j = j*j;
        j = j/j;
        j = n - j;
    }
}

double callTest(int size, int sortAlg, int testIndex, double args[], int smallN) {
    if (testIndex == 0) {
        return testRandom(size, sortAlg, smallN);
    } else if (testIndex == 1) {
        return testSorted(size, sortAlg, smallN);
    } else if (testIndex == 2) {
        return testPartiallySorted(size, sortAlg, args[0], smallN);
    } else if (testIndex == 3) {
        return testSwaps(size, sortAlg, args[0], smallN);
    } else if (testIndex == 4) {
        return testPartiallySorted2(size, sortAlg, args[0], smallN);
    } else if (testIndex == 5) {
        return testSortedBackwards(size, sortAlg, smallN);
    } else if (testIndex == 6) {
        return testEqual(size, sortAlg, smallN);
    } else if (testIndex == 7) {
        return testPartiallySorted3(size, sortAlg, args[0], smallN);
    }
}

// Start sort test section

double testRandom(int size, int sortAlg, int smallN) {
    int *A;
    A = malloc(size * sizeof(int));
    for (int i = 0; i < size; i++){
        int r = rand() % size;
        A[i] = r;
    }
    return sortPrecall(A, size, sortAlg, smallN);
}

double testSorted(int size, int sortAlg, int smallN) {
    int *A;
    A = malloc(size * sizeof(int));
    for (int i = 0; i < size; i++){
        A[i] = i;
    }
    return sortPrecall(A, size, sortAlg, smallN);
}

double testPartiallySorted(int size, int sortAlg, double sortedPercentage, int smallN) {
    int *A;
    A = malloc(size * sizeof(int));
    int eI = (int)(sortedPercentage * size);
    for (int i = 0; i < size; i++){
        if (i < eI){
            A[i] = i;
        } else {
            int r = rand() % size;
            if (r >= i){
                r = -r;
            }
            A[i] = r;
        }
    }
    return sortPrecall(A, size, sortAlg, smallN);
}

double testSwaps(int size, int sortAlg, double numberOfSwaps, int smallN) {
    int *A;
    A = malloc(size * sizeof(int));
    for (int i = 0; i < size; i++){
        A[i] = i;
    }
    int *C;
    C = malloc(size * sizeof(int));
    for (int i = 0; i < numberOfSwaps; i++){
        int rI = rand() % size;
        while (quickContains(rI, C, size) == 1) {
            rI = rand() % size;
        }
        int rI2 = rand() % size;
        while (rI == rI2 && (quickContains(rI2, C, size)) == 1)  {
            rI2 = rand() % size;
        }
        C[i*2] = rI;
        C[i*2 + 1] = rI2; 
        int t = A[rI];
        A[rI] = A[rI2];
        A[rI2] = t;
    }
    free(C);
    return sortPrecall(A, size, sortAlg, smallN);
}

double testPartiallySorted2(int size, int sortAlg, double stepSize, int smallN) {
    int *A;
    A = malloc(size * sizeof(int));
    int i = 0;
    int pV = 0;
    while (i < size) {
        int r = rand() % size;
        for (int j = 0; j < stepSize; j++) {
            A[i] = r + j;
            i++;
            if (i >= size) break;
        }
    }
    return sortPrecall(A, size, sortAlg, smallN);
}

double testSortedBackwards(int size, int sortAlg, int smallN){
    int *A;
    A = malloc(size * sizeof(int));
    for (int i = 0; i < size; i++){
        A[i] = size - i - 1;
    }
    return sortPrecall(A, size, sortAlg, smallN);
}

double testEqual(int size, int sortAlg, int smallN) {
    int *A;
    A = malloc(size * sizeof(int));
    for (int i = 0; i < size; i++){
        A[i] = size - 1;
    }
    return sortPrecall(A, size, sortAlg, smallN);
}


double testPartiallySorted3(int size, int sortAlg, double numOfSteps, int smallN) {
    int *A;
    A = malloc(size * sizeof(int));
    int i = 0;
    while (i < size) {
        int t = (rand() % size);
        t *= numOfSteps;
        for (int j = t/numOfSteps; j < 1/numOfSteps + t/numOfSteps; j++) {
            if (i >= size) {
                break;
            }
            A[j] = i;
            i++;
        }
    }
    return sortPrecall(A, size, sortAlg, smallN);
}


// End sort test section

double sortPrecall(int A[], int size, int sortAlg, int smallN) {
    int runs = 1;
    if (smallN == 2) runs = TC*TC;
    if (smallN == 1) runs = TC;
    clock_t start_t, end_t, total_t;
    for (int i = 0; i < size; i++) {
        A[i] = abs(A[i]) % size;
    }

    int *B;
    int *C;
    B = malloc(size * sizeof(int));
    C = malloc(size * sizeof(int));
    start_t = clock();
    for (int run = 0; run < runs; run++){
        for (int i = 0; i < size; i++) {
            B[i] = 0;
        }
        copy_array(A, C, size);
        callSort(A, size, sortAlg, B);
    }

    end_t = clock();
    total_t = (double)(end_t - start_t) / (double)(CLOCKS_PER_SEC) * ACC;
    free(A);
    free(B);
    free(C);
    return total_t; 
}

