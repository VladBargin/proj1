#include <stdlib.h>
#include "utils.h"
#include "sorts.h"
#include <stdio.h>

 
#define MINRUN 32
#define multiply( f1, f2 ) ( f1 * f2 )
#define i_parent( i ) ( (int) floor((i-1) / 2) )
#define i_left_child( i ) ( 2*i + 1 )
#define i_right_child( i ) ( 2*i + 2 )

void dummy(int A[]) {
    
}

// array A[] has the items to sort; array B[] is a work array
void merge_sort(int A[], int B[], int n)
{
    for (int width = 1; width < n; width = 2 * width)
    {
        for (int i = 0; i < n; i = i + 2 * width)
        {
            bottom_up_merge(A, i, min(i+width, n), min(i+2*width, n), B);
        }
        copy_array(B, A, n);
    }
}
//  Left run is A[iLeft :iRight-1].
// Right run is A[iRight:iEnd-1  ].
void bottom_up_merge(int A[], int iLeft, int iRight, int iEnd, int B[])
{
    int i = iLeft;
    int j = iRight;
    // While there are elements in the left or right runs...
    for (int k = iLeft; k < iEnd; k++) {
        // If left run head exists and is <= existing right run head.
        if (i < iRight && (j >= iEnd || A[i] <= A[j])) {
            B[k] = A[i];
            i = i + 1;
        } else {
            B[k] = A[j];
            j = j + 1;    
        }
    }
}

void bubble_sort(int A[], int size) {
    if (size <= 1) {
        return;
    }
    int n = size;
    while (n > 0) {
        int swapped = 0;
        for (int i = 1; i < n; i++) {
            if (A[i-1] > A[i]) {
                swap(A, i-1, i);
                swapped = 1;
            }
        }
        if (!swapped) {
            n = n - 1;
        }
    }
}

void shell_sort1(int A[], int B[], int size) {
    if (size <= 1) {
        return;
    }
    int k = 1;
    //int* gaps;
    int v;
    while (k < size) {
        v = ssf1(k);
        if (v > size) {
            break;
        } else {
            k++;
        }
    }

    //gaps = malloc(k * sizeof(int));
    B[0] = 1;
    for (int y = 2; y < k + 1; y += 1) {
        v = ssf1(y - 1);
        B[y - 1] = v;
    }
    reverse(B, k - 1);

    for(int x = 0; x < k - 1; x++) {
        int gap = B[x];
        for (int i = gap; i < size; i += 1)
        {
            int temp = A[i];
            int j;
            for (j = i; j >= gap && A[j - gap] > temp; j -= gap)
            {
                A[j] = A[j - gap];
            }
            A[j] = temp;
        }
    }
    //free(gaps);
}

void shell_sort2(int A[], int B[], int size) {
    if (size <= 1) {
        return;
    }
    int k = 1;
    //int* gaps;
    int v;
    while (k < size) {
        v = ssf2(k); 
        if (v >= size) {
            break;
        } else {
            k++;
        }
    }
    //gaps = malloc(k * sizeof(int));
    for (int y = 1; y < k; y += 1) {
        v = ssf2(y);
        B[k - y - 1] = v;
    }
    //reverse(B, k - 1);
    for(int x = 0; x < k - 1; x++) {
        int gap = B[x];
        for (int i = gap; i < size; i += 1)
        {
            int temp = A[i];
            int j;
            for (j = i; j >= gap && A[j - gap] > temp; j -= gap)
            {
                A[j] = A[j - gap];
            }
            A[j] = temp;
        }
    }
    //free(gaps);
}

void insertion_sort(int A[], int size){
    ins_sort(A, 0, size - 1);
}

void ins_sort(int A[], int left, int right) { 
    for (int i = left + 1; i <= right; i++) {
        int j = i;
        while (j > left && A[j-1] > A[j]) {
            swap(A, j, j - 1);
            j--;
        }
    }
}

void counting_sort(int A[], int B[], int size) {
    for (int i = 0; i < size; i++)
        B[A[i]] += 1;

    int i = 0;
    int j = 0;
    while (i < size) {
        if (B[j] == 0) {
            j++;
        } else {
            A[i] = j;
            B[j] -= 1;
            i++;
        }
    }
}

void quicksort(int A[], int size) {        
    dual_pivot_quicksort(A, 0, size - 1, 3); 
}

void dual_pivot_quicksort(int a[], int left,int right, int divid) {
    int len = right - left;
    if (len < 27) { // insertion sort for tiny array
        ins_sort(a, left, right);
        return;
    }
    int third = len / divid;

    int m1 = left  + third;    
    int m2 = right - third;
    if (m1 <= left) { m1 = left + 1; }
    if (m2 >= right) { m2 = right - 1; }
    if (a[m1] < a[m2]) { 
        swap(a, m1, left); 
        swap(a, m2, right);    
    } else {        
        swap(a, m1, right);        
        swap(a, m2, left);    
    }

    int pivot1 = a[left];    
    int pivot2 = a[right];

    int less  = left  + 1;    
    int great = right - 1;

    for (int k = less; k <= great; k++) {
        if (a[k] < pivot1) {            
            swap(a, k, less++);        
        } else if (a[k] > pivot2) {
            while (k < great && a[great] > pivot2) {
                great--;            
            }
            swap(a, k, great--);
            if (a[k] < pivot1) { swap(a, k, less++); }
        }
    }

    int dist = great - less;
    if (dist < 13) {
        divid++;
    }

    swap(a, less-1,left);
    swap(a, great + 1, right);

    dual_pivot_quicksort(a, left, less - 2, divid);
    dual_pivot_quicksort(a, great + 2, right, divid);

    if (dist > len - 13 && pivot1 != pivot2) {
        for (int k = less; k <= great; k++) {
            if (a[k] == pivot1) {
                swap(a, k, less++);
            } else if (a[k] == pivot2) {
                swap(a, k, great--);

                if(a[k] == pivot1) {
                    swap(a, k, less++);
                }
            }
        }
    }

    if (pivot1 < pivot2) {
        dual_pivot_quicksort(a, less, great, divid);
    }
}

void heapsort(int A[], int size) {
    if (size <= 1) {
        return;
    }
    heapify(A, size);
    int end = size - 1;
    while (end > 0) {
        swap(A, end, 0);
        end -= 1;
        sift_down(A, 0, end);
    }
}

void heapify(int A[], int size) {
    int start = i_parent(size-1);
    while (start >= 0) {
        sift_down(A, start, size - 1);
        start -= 1;
    }
}

void sift_down(int A[], int i, int end) {
    int j = leaf_search(A, i, end);
    while (A[i] > A[j]) {
        j = i_parent(j);
    }
    int x = A[j];
    A[j] = A[i];
    while (j > i) {
        int t = x;
        x = A[i_parent(j)];
        A[i_parent(j)] = t;
        j = i_parent(j);
    }
}

int leaf_search(int A[], int i, int end) {
    int j = i;
    while (i_right_child(j) <= end) {
        if (A[i_right_child(j)] > A[i_left_child(j)]) {
            j = i_right_child(j);
        } else {
            j = i_left_child(j); 
        }
    }
    if (i_left_child(j) <= end) {
        j = i_left_child(j);
    }
    return j;
}

void tim_sort(int A[], int B[], int n) { 
    for (int i = 0; i < n; i+=MINRUN) { 
        ins_sort(A, i, min((i+MINRUN-1), (n-1)));
    }

    for (int size = MINRUN; size < n; size = 2*size) 
    {
        
        for (int left = 0; left < n; left += 2*size) 
        { 
            int mid = left + size - 1; 
            int right = min((left + 2*size - 1), (n-1)); 
            bottom_up_merge(A, left, mid + 1, right + 1, B); 
        }
        copy_array(B, A, n);
    }
}

/* void t_s(int A[], int B[], int l, int r) {
} *//* 

void mysort(int A[], int n){
    for (int i = 0; i < n; i+=MINRUN) { 
        ins_sort(A, i, min((i+MINRUN-1), (n-1)));
    }
    for (int i = MINRUN; i < n; i*=2) { 
        ins_sort(A, 0, min((i+MINRUN-1), (n-1)));
    }
} */

