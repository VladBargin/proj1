#include "sorts.h"
#include "utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define MAXSORTINDEX 9

int sorted(int A[], int size);
int equalArr(int A[], int B[], int size);

int main(int argc,  char** argv) {
    char ti = '0';
    if (argc > 0) {
        ti = argv[1][0];
    }
    int ret = 0;
    if (ti == '0') {
        printf("Shallow test for sorts\n");
        int size = 10000;
        for (int i = 0; i <= MAXSORTINDEX; i++) {
            int *A;
            int *B;
            A = malloc(size * sizeof(int));
            B = malloc(size * sizeof(int));
            srand(time(NULL)); 
            for (int i = 0; i < size; i++){
                int r = rand() % size;
                A[i] = r;
                B[i] = 0;
            }
            callSort(A, size, i, B);
            int s = sorted(A, size);
            free(A);
            free(B);
            if (s < 0) {
                ret = -1;
                printf("Sort with index: %i failed\n", i);
            }
        }
    } else if (ti == '1') {
        printf("Test for seg faults\n");
        int size = 100;
        for (size = 100; size < 1000; size++) {
            int *A;
            int *B;
            A = malloc(size * sizeof(int));
            B = malloc(size * sizeof(int));
            for (int i = 0; i <= MAXSORTINDEX; i++) {
                srand(time(NULL)); 
                for (int i = 0; i < size; i++){
                    int r = rand() % size;
                    A[i] = r;
                    B[i] = 0;
                }
                callSort(A, size, i, B);
                
            }
            free(A);
            free(B);
        }
    } else if (ti == '2') {
        printf("Deeper test for sorts\n");
        int size = 100;
        int D[20];
        for (int z = 0; z < 20; z += 1) {
            D[z] = -1;
        } 
        for (size = 100; size <= 2000; size += 10) {
            int *A;
            int *B;
            int *C;
            A = malloc(size * sizeof(int));
            B = malloc(size * sizeof(int));
            C = malloc(size * sizeof(int));
            for (int i = 0; i <= MAXSORTINDEX; i++) {
                while (i <= MAXSORTINDEX) {
                    if (D[i] > 0) {
                        i += 1;
                    } else {
                        break;
                    }
                }
                if (i > MAXSORTINDEX) {
                    break;
                }
                srand(time(NULL)); 
                for (int i = 0; i < size; i++){
                    int r = rand() % size;
                    A[i] = r;
                    C[i] = r;
                    B[i] = 0;
                }
                callSort(A, size, i, B);
                qsort(C, size, sizeof *C, comp);
                int s = equalArr(A, C, size);
                if (s < 0) {
                    if (D[i] < 0) {
                        printf("Sort with index: %i failed\n", i);
                    }
                    D[i] = 1;
                    ret = -1;
                }
            }
            free(A);
            free(B);
        }
    } else if (ti == '3') {
        printf("Deepest test for sorts\n");
        int D[20];
        for (int z = 0; z < 20; z += 1) {
            D[z] = -1;
        } 
        for (int size = 1000; size <= 10000; size += 500) {
            printf("\nSIZE: %i", size);
            fflush(stdout);
            int *A;
            int *B;
            int *C;
            A = malloc(size * sizeof(int));
            B = malloc(size * sizeof(int));
            C = malloc(size * sizeof(int));
            for (int i = 0; i <= MAXSORTINDEX; i++) {
                while (i <= MAXSORTINDEX) {
                    if (D[i] > 0) {
                        i += 1;
                    } else {
                        break;
                    }
                }
                if (i > MAXSORTINDEX) {
                    break;
                }
                srand(time(NULL)); 
                for (int i = 0; i < size; i++){
                    int r = rand() % size;
                    A[i] = r;
                    C[i] = r;
                    B[i] = 0;
                }
                callSort(A, size, i, B);
                qsort(C, size, sizeof *C, comp);
                int s = equalArr(A, C, size);
                if (s < 0) {
                    if (D[i] < 0) {
                        printf("Sort with index: %i failed on random test\n", i);
                    }
                    D[i] = 1;
                    ret = -1;
                }

                for (int i = 0; i < size; i++){
                    A[i] = size - i - 1;
                    C[i] = A[i];
                    B[i] = 0;
                }

                callSort(A, size, i, B);
                qsort(C, size, sizeof *C, comp);
                s = equalArr(A, C, size);
                if (s < 0) {
                    if (D[i] < 0) {
                        printf("Sort with index: %i failed on back sorted test\n", i);
                    }
                    D[i] = 1;
                    ret = -1;
                }

            }
            free(A);
            free(B);
        }
    }
    
    return ret;
}

int equalArr(int A[], int B[], int size) {
    for (int i = 0; i < size; i++) {
        if (A[i] != B[i]) {
            return -1;
        }
    }
    return 1;
}

int sorted(int A[], int size) {
    for (int i = 1; i < size; i++) {
        if (A[i-1] > A[i]) {
            return -1;
        }
    }
    return 0;
}