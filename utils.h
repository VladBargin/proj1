#ifndef UTILS_H_
#define UTILS_H_

#include <math.h>
#include "sorts.h"
#include <stdlib.h>

int quickContains(int el, int A[], int size);
void copy_array(int B[], int A[], int size);
void copy_arr(int B[], int A[], int left, int right);
int min(int x, int y);
void swap(int A[], int i, int j);
double powF(double x, int y);
void reverse(int A[], int size);
int roundUp(double a);
int ssf1(int y);
int ssf2(int y);
int abs(int a);
int maxA(int A[], int size);
int comp (const void * elem1, const void * elem2);
void callSort(int A[], int size, int sortAlg, int B[]);
/* int set_es(struct Stack *stack);
int pop(struct Stack *stack);
void push(struct Stack *stack, int el);
int get_el(struct Stack *stack, int ife);


struct Pair {
    int size;
    int* A;
};

struct Stack {
    unsigned int empty:1;
    int cur_el;
    struct Pair *elem;
}; */
#endif