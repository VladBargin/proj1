#include "utils.h"
#include <math.h>

int quickContains(int el, int A[], int size){
    for (int i = 0; i < size; i++){
        if (A[i] == el) {
            return 1;
        } else if (A[i] == 0) {
            return 0;
        }
    }
    return 0;
}

void copy_array(int B[], int A[], int size) {
    copy_arr(B, A, 0, size - 1);
}

void copy_arr(int B[], int A[], int left, int right) {
    for(int i = left; i <= right; i++) {
        A[i] = B[i];
    }
}

int min(int x, int y){
    return (x < y) ? x : y;
}

void swap(int A[], int i, int j) {
    int t = A[i];
    A[i] = A[j];
    A[j] = t;
}

double powF(double x, int y) {
    int res = 1;
    while (y > 0) {
        res *= x;
        y -= 1;
    }
    return res;
}

void reverse(int A[], int size) {
    for (int i = 0; i < size / 2; i++) {
        swap(A, i, size - i - 1);
    }
}

int roundUp(double a) {
    if ((int)a < a) {
        return (int) (a) + 1;
    } else {
        return (int) (a);
    }
}

int ssf1(int y){
    return (int)powF(4, y) + 3*(int)powF(2, y-1) + 1;
}

int ssf2(int y){
    return roundUp( (9.0 * powF(9.0/4.0, y - 1) - 4.0) / 5.0 );
}

int abs(int a){
    if (a < 0) return -a;
    else return a;
}

int maxA(int A[], int size) {
    int m = A[0];
    for (int i = 1; i < size; i++) {
        if (A[i] > m) m = A[i];
    }
    return m;
}

int comp (const void * elem1, const void * elem2) 
{
    int f = *((int*)elem1);
    int s = *((int*)elem2);
    if (f > s) return  1;
    if (f < s) return -1;
    return 0;
}

/* int floor(double x) {
    int y = (int) x;
    return y;
} */

void callSort(int A[], int size, int sortAlg, int B[]){
    if (sortAlg == 0) {
        merge_sort(A, B, size);
    } else if (sortAlg == 1) {
        bubble_sort(A, size);
    } else if (sortAlg == 2) {
        shell_sort1(A, B, size);
    } else if (sortAlg == 3) {
        shell_sort2(A, B, size);
    } else if (sortAlg == 4) {
        insertion_sort(A, size);
    } else if (sortAlg == 5) {
        counting_sort(A, B, size);
    } else if (sortAlg == 6) {
        quicksort(A, size);
    } else if (sortAlg == 7) {
        heapsort(A, size);
    } else if (sortAlg == 8) {
        tim_sort(A, B, size);
    } else if (sortAlg == 9) {
        qsort(A, size, sizeof *A, comp);
    } else {
        dummy(A);
    }
}



// Stack

/* int set_es(struct Stack *stack) {
    if (stack->cur_el <= -1) {
        stack->empty = 1;
    } else {
        stack->empty = 0;
    }
}

struct Pair pop(struct Stack *stack) {
    struct Pair retVal = stack->elem[stack->cur_el];
    stack->cur_el -= 1;
    set_es(stack);
    return retVal;
}

void push(struct Stack *stack, int el) {
    stack->cur_el += 1;
    stack->elem[stack->cur_el] = el;
    set_es(stack);
}

int get_el(struct Stack *stack, int ife) {
    if (ife > stack->)
    return stack->elem[stack->cur_el - ife];
} */